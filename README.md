**Readme file for PwEstimator**
The database installation is shown here: https://youtu.be/3sS6baLt5ko
The files shown and required in the video can be found here: https://drive.google.com/drive/folders/1On1CuRlYrZTeFX2gsufTTR4LjE4cvZL_?usp=sharing

During isntallation Pyodbc and Flask required to be downloaded via the pip function with python.

To use the software lauch the PwEstimator.exe (please note that this is hard coded to connect to localhost\MSSQLSERVER01, if the server does not match that connection will be refused. Changing that is possible in the source code. For both the API and PwEstimator.

To use the API the source code should be compiled by opening it in Python and running it which then will host a server on localnetwork under https://127.0.0.1:8001/. The .pem files included in the previous google drive are crucial, they need to be in the same folder as the API Source.py.

API can only be used with Postman, posting a password with browser is not possible, to post with Postman use https://127.0.0.1:8001/submit?password=password .

Source Code and Version Control can be found here: https://bitbucket.org/rRoots/pwestimator-source/src/master/ 