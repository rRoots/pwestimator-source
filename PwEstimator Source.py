import pyodbc
from tkinter import *
from tkinter import scrolledtext
from tkinter.ttk import Progressbar
import hashlib
import re


conn = pyodbc.connect('Driver={SQL Server};' #Database connection
                      'Server=localhost\MSSQLSERVER01;'
                      'Database=passwords;'
                      'Trusted_Connection=yes;')

#Defining cursor for the database
cursor = conn.cursor()


# Window Object
app = Tk()
app.title('PwEstimator')
app.geometry('720x480')

#Text field
part_text = StringVar()
part_entry = Entry(app, textvariable=part_text)
part_entry.grid(column=0, row=1, pady=40, padx=40)

#button
def clicked():
    result = part_text.get().strip()#Logic to filter out certain special characters
    if result.find('$')!=-1 or result.find('\'')!=-1 or result.find('@')!=-1 or result.find('#')!=-1 or result.find('/')!=-1 or result.find('¬')!=-1 \
    or result.find('/')!=-1 or result.find('?')!=-1 or result.find('\'')!=-1 or result.find(',')!=-1 or result.find(';')!=-1 or result.find('~')!=-1 \
    or result.find('<')!=-1 or result.find('>')!=-1 or result.find('£')!=-1 or result.find('"')!=-1 or result.find('%')!=-1 or result.find('^')!=-1:
        txt.insert(INSERT,'Don\'t use certain special characters. ' +result + '\n')#no results
    elif len(result)> 39: #Triggered if the password is longer than 39 characters, 40 characters is the typical length for sha-1
        cursor.execute('SELECT * FROM [passwords].[dbo].[Sha1Passwords] WHERE (column1) LIKE ?', (result+'%',))
        
        res=cursor.fetchall()
        if len(res)==0:
            txt.insert(INSERT,'Entry is not in database: ' +result + '\n')#no results
        else:
            txt.insert(INSERT,'Entry is in database: ' +result + '\n')#results
    else:
        cursor.execute('SELECT * FROM [passwords].[dbo].[millionsofpasswrds]  WHERE (column1) = ?', (result,))
        res=cursor.fetchall()
        if len(res)==0:
            txt.insert(INSERT,'Entry is not in database: ' +result + '\n')#no results
        else:
            txt.insert(INSERT,'Entry is in database: ' +result + '\n')#results
            
def hashed():#generates the hash
    result = part_text.get().strip()
    encodeHash = result.encode("utf-8")
    sha1Hash = hashlib.sha1(encodeHash).hexdigest()
    txt.insert(INSERT,'The Sha-1 hash for ' +result + ' is: ' +sha1Hash + '\n')

def entropy():
    result = part_text.get().strip()
    asciicharset= 95-18# 18 is the special characters that are removed from the traditional 95 ascii
    regexSpecial = re.compile('[^0-9a-zA-Z]+') #define all 62 possible characters to find missing special characters
    isUpper = any(element.isupper() for element in result) #finds any upper cases
    isLower = any(element.islower() for element in result) #finds any lower cases
    isDigit = any(element.isdigit() for element in result) #finds any digits
    if isUpper == False:
        asciicharset = asciicharset - 26
    if isLower == False:
        asciicharset = asciicharset - 26
    if isDigit == False:
        asciicharset = asciicharset - 10
    if not regexSpecial.search(result):
        asciicharset = asciicharset - 15


    #calulation
    passBits= asciicharset**len(result)
    txt.insert(INSERT,'The possible combinations for '+ result +' is ' + str(passBits) + '\n')

    #calculates crack time based on an average computer and sha1 format
    crackTime = passBits / 22000000000
    crackOut = int(crackTime)
    txt.insert(INSERT,'A typical computer could crack the password in '+ str(crackOut)+ ' hours with bruteforcing.' + '\n')
    progressbar = ((passBits - 0) * 100) / (30000000000000 - 0)
    bar['value'] = progressbar



def clear():
    txt.delete('1.0', END)

#GUI objects
btn = Button(app, text='Submit Password', command=clicked)
btn.grid(column=0,row=3)
btnClear = Button(app, text='Clear Text', command=clear)
btnClear.grid(column=6,row=3)
btnHash = Button(app, text='Generate Sha-1 Hash', command=hashed)
btnHash.grid(column=0, row=4, pady=10)
btnEntro = Button(app, text='Generate Password Entropy', command=entropy)
btnEntro.grid(column=0, row=5)


#Scroll widget
txt = scrolledtext.ScrolledText(width=60,height=15)
txt.grid(column=6,row=5)


#Progress Bar
bar = Progressbar(length=200)
bar['value'] = 0
bar.grid(column=6, row=6)


# Start the program
app.mainloop()
