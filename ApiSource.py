from flask import Flask, request, jsonify
import flask
import pyodbc
import re
conn = pyodbc.connect('Driver={SQL Server};' #Database connection
                      'Server=localhost\MSSQLSERVER01;'
                      'Database=passwords;'
                      'Trusted_Connection=yes;')


cursor = conn.cursor() #Defining cursor for the database


app = flask.Flask(__name__) #Flask instance

@app.route('/submit', methods=['POST']) 
def submit():
    if request.method == 'POST':
       passRequest = request.args.get('password', '')
       print(passRequest)
       #Logic to filter out certain special characters
    if passRequest.find('$')!=-1 or passRequest.find('\'')!=-1 or passRequest.find('@')!=-1 or passRequest.find('#')!=-1 or passRequest.find('/')!=-1 or passRequest.find('¬')!=-1 \
    or passRequest.find('/')!=-1 or passRequest.find('?')!=-1 or passRequest.find('\'')!=-1 or passRequest.find(',')!=-1 or passRequest.find(';')!=-1 or passRequest.find('~')!=-1 \
    or passRequest.find('<')!=-1 or passRequest.find('>')!=-1 or passRequest.find('£')!=-1 or passRequest.find('"')!=-1 or passRequest.find('%')!=-1 or passRequest.find('^')!=-1:
        returnMsg = 'Dont\'t use certain special characters. '+ '\n'
    elif len(passRequest)> 39: #Triggered if the password is longer than 39 characters, 40 characters is the typical length for sha-1
        cursor.execute('SELECT * FROM [passwords].[dbo].[Sha1Passwords] WHERE (column1) LIKE ?', (passRequest+'%',))
        if len(passRequest)==0:
            returnMsg = 'Password is not in the database. ' + '\n'
        else:
            returnMsg = 'Password is in the database. ' + '\n'
        returnEntropy =''
        returnEntropyTime=''
    else:
        asciicharset= 95-18# 18 is the special characters that are removed from the traditional 95 ascii
        regexSpecial = re.compile('[^0-9a-zA-Z]+') #define all 62 possible characters to find missing special characters
        isUpper = any(element.isupper() for element in passRequest) #finds any upper cases
        isLower = any(element.islower() for element in passRequest) #finds any lower cases
        isDigit = any(element.isdigit() for element in passRequest) #finds any digits
        if isUpper == False:
            asciicharset = asciicharset - 26
        if isLower == False:
            asciicharset = asciicharset - 26
        if isDigit == False:
            asciicharset = asciicharset - 10
        if not regexSpecial.search(passRequest):
            asciicharset = asciicharset - 15


        #calulation
        passBits= asciicharset**len(passRequest)
        returnEntropy = 'The possible combinations for '+ passRequest +' is ' + str(passBits) + '\n'

        #calculates crack time based on an average computer and sha1 format
        crackTime = passBits / 22000000000
        crackOut = int(crackTime)
        returnEntropyTime ='A typical computer could crack the password in '+ str(crackOut)+ ' hours with bruteforcing.' + '\n'
        #
        cursor.execute('SELECT * FROM [passwords].[dbo].[millionsofpasswrds]  WHERE (column1) = ?', (passRequest,))
        res=cursor.fetchall()#Gets all the output
        if len(res)==0:
            returnMsg = 'Password is not in the database. ' + '\n'
        else:
            returnMsg = 'Password is in the database. ' + '\n'
            
    return returnMsg  + returnEntropy + returnEntropyTime

    
    #Simple readme via the api
@app.route('/', methods=['GET'])
def home():
    return """
    <h1>README.md</h1>
    <h2>To request password analysis send password to /submit </h2>
    <h2>for example submit?password=password1 </h2>
    """

#Generated certifications and the port
app.run(ssl_context=('cert.pem', 'key.pem'),port=8001 )

